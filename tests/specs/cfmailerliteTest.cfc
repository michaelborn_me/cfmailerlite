/**
* Big honking test suite for mailerlite wrapper.
*/
component extends="testbox.system.BaseSpec"{

	/*********************************** LIFE CYCLE Methods ***********************************/
	
		function beforeAll(){
			// we need to mock Hyper so that we don't actually submit the API request during test.
			// Then we inject our "fake" hyper component and run the test.
			// Then our expectation is that the base url, endpoint, api key,
			// content type, body, etc. are set correctly. :)
			application.hyper = getMockBox().createMock( "hyper.models.HyperBuilder" );
			application.cfmailerlite = new models.cfmailerlite();
			application.cfmailerlite.setHyper( application.hyper );
		}
	
		function afterAll(){
			structClear( application );	
		}
	
	/*********************************** BDD SUITES ***********************************/
	
		function run(){
	
			/** 
			 * uh...
			*/
			describe( "cfmailerlite", function(){
				it( "sets the API URL and key correctly", function(){
					application.cfmailerlite.setBaseURL( "myAPI" );
					application.cfmailerlite.setAPIKey( "KEY_1234" );
					debug( application.cfmailerlite.getHyper() );
					expect( application.cfmailerlite.getHyper().getHeaders() ).toBe( {
						"Content-Type"       : "application/json",
						"X-MailerLite-ApiKey": "KEY_1234"
					} );
				} );
				// it( "can get groups", function(){
				// 	var response = application.cfmailerlite
				// 		.getGroups();
				// 	expect( response ).toBeTypeOf( "component" );
				// 	expect( response.getURL() ).toBe( "/groups" );
				// } );
			} );
		}
}