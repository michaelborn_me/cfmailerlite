component accessors="true" {

	property name="baseURL" inject="box:setting:baseURL@cfMailerlite";
	property name="apiKey" inject="box:setting:apiKey@cfMailerlite";
	property name="hyper" inject="HyperBuilder@Hyper";

	/**
	 * Store the HTTP response after each call.
	 * This way we can return self (more chainable)
	 * and let the user grab status code or whatever.
	*/
	property name="_response" type="component";

	/**
	 * Initialize HyperBuilder and pass in our API key and base URL.
	*/
	public component function getHyper(){
		return variables.hyper
			.defaults.setBaseURL( variables.baseURL )
			.withHeaders( {
				"Content-Type"       : "application/json",
				"X-MailerLite-ApiKey": variables.apiKey
			} );
	}

	/**
	 * Given an HTTP response, eithr throw an error or return data by converting from JSON to a struct.
	*/
	public any function wrapResponse( component HyperRequest ){
		setResponse( arguments.HyperRequest );
		
		var statusCode = getResponse().getStatusCode();
		if ( statusCode > 299 ){
			cfthrow( message = "Error in request: #getJSON()#" );
		}
		return this;
	}

	public any function getStatusCode(){
		if ( !isValid("cfc", getResponse()) ){
			cfthrow( message="No response found" );
		}
		return getResponse().getStatusCode();
	}

	public any function getJSON(){
		if ( !isValid("cfc", getResponse()) ){
			cfthrow( message="No response found" );
		}
		var data = getResponse.getData();
		if ( !isJson( data ) ) {
			cfthrow( message="Invalid JSON in response body" );
		}
		return data = deserializeJSON( data );
	}
}