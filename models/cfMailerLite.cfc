component extends="BaseSDK"{

	/**
	 * Get subscriber groups - groups are like a category
	 * @id Get single group by ID
	 * @cite https://developers.mailerlite.com/reference#groups
	*/
	public function getGroups( string id = "" ){
		var endpoint = "/groups";
		if ( arguments.id > "" ){
			endpoint &= "/#encodeForURL(arguments.id)#";
		}
		var response = getHyper()
			.get( endpoint );
		return wrapResponse( response );
	}
	/**
	 * Subscribe an email address to a known group.
	 * @id ID of group - required to specify the group to subscribe to.
	 * @body body parameters - will be JSON-serialized before submitting.
	 * @cite https://developers.mailerlite.com/reference#add-single-subscriber
	*/
	public function addSubscriberToGroup( required string id = "", required struct body ){
		var endpoint = "/groups/#encodeForURL(arguments.id)#/subscribers";
		var response = getHyper()
			.post(
				url = endpoint,
				body = arguments.body
			);
		return wrapResponse( response );
	}

	/**
	 * Get subscriber segments - segments are like a filter
	*/
	public function getSegments(){
		var endpoint = "/segments";
		return wrapResponse( getHyper().get( endpoint ) );
	}

	/**
	 * Retrieve subscribers
	 * @id Get single subscriber by id
	*/
	public function getSubscribers( string id = "", string email = "" ){
		var endpoint = "/subscribers";
		if ( arguments.id > "" ){
			endpoint &= "/#encodeForURL(arguments.id)#";
		} else if ( arguments.email > "" ){
			endpoint &= "/#encodeForURL(arguments.email)#";
		}
		return wrapResponse( getHyper().get( endpoint ) );
	}
}