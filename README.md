# cfMailerLite

A CFML wrapper for the [MailerLite API](https://developers.mailerlite.com/reference).

## Getting Started: ColdBox Setup

1. Install this module: `box install cfmailerlite`
2. Obtain an [API key](https://developers.mailerlite.com/docs/authentication) from MailerLite.
3. Place the API key in your `config/Coldbox.cfc` like so:

```js
moduleSettings = {
	cfmailerlite: {
		apiKey: server.system.environment.CFMAILERLITE_API_KEY
	}
};
```

Then inject `cfmailerlite` to use the API:

```js
property name="cfmailerlite" inject="cfmailerlite@cfmailerlite";
```

## Getting Started: Vanilla CFML Setup


1. Install this module: `box install cfmailerlite`
2. Obtain an [API key](https://developers.mailerlite.com/docs/authentication) from MailerLite.
3. Instantiate the model: 

```js
var cfmailerlite = new modules.cfmailerlite.models.cfmailerlite();
```

4. Set the API key and base API url:

```js
var cfmailerlite = new modules.cfmailerlite.models.cfmailerlite()
	.setBaseURL( "https://api.mailerlite.com/api/v2/" )
	.setAPIKey( server.system.environment.CFMAILERLITE_API_KEY );
```

## Supported API Endpoints

### Subscriber Groups

* `GET /groups` - use `cfmailerlite.getGroups()`
* `GET /groups/:id` - use `cfmailerlite.getGroups( id="123z" )`

### Segments

* `GET /segments` - use `cfmailerlite.getSegments()`

### Subscribers

* `GET /subscribers` - use `cfmailerlite.getSubscribers()`
* `GET /subscribers/:id` - use `cfmailerlite.getSubscribers( id="1234x" )`
* `GET /subscribers/:email` - use `cfmailerlite.getSubscribers( email="mike@learncf.dev" )`

### Adding Subscribers to a Group

* `POST /groups/:id/subscribers` - use `cfmailerlite.addSubscriberToGroup( id="", body={email="mike@learncf.dev"} )`

## TODO

* ❌ Finish writing tests
* ❌ Add full API support

## The Good News

> For all have sinned, and come short of the glory of God ([Romans 3:23](https://www.kingjamesbibleonline.org/Romans-3-23/))

> But God commendeth his love toward us, in that, while we were yet sinners, Christ died for us. ([Romans 5:8](https://www.kingjamesbibleonline.org/Romans-5-8))

> That if thou shalt confess with thy mouth the Lord Jesus, and shalt believe in thine heart that God hath raised him from the dead, thou shalt be saved. ([Romans 10:9](https://www.kingjamesbibleonline.org/Romans-10-9/))
 
## Repository

Copyright 2019 (and on) - [Michael Born](https://michaelborn.me/)

* [Homepage](https://bitbucket.org/michaelborn_me/cfmailerlite/)
* [Issue Tracker](https://bitbucket.org/michaelborn_me/cfmailerlite/issues?status=new&status=open)
* [New BSD License](https://opensource.org/licenses/BSD-3-Clause)

[![cfmlbadges](https://cfmlbadges.monkehworks.com/images/badges/made-with-cfml.svg)](https://cfmlbadges.monkehworks.com) [![cfmlbadges](https://cfmlbadges.monkehworks.com/images/badges/tested-with-testbox.svg)](https://cfmlbadges.monkehworks.com) [![cfmlbadges](https://cfmlbadges.monkehworks.com/images/badges/powered-by-coffee.svg)](https://cfmlbadges.monkehworks.com) [![cfmlbadges](https://cfmlbadges.monkehworks.com/images/badges/i-can-bench-press-ben-nadel.svg)](https://cfmlbadges.monkehworks.com)