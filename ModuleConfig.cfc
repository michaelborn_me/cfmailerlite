component {
	this.title        = "cfMailerLite";
	this.description  = "CFML wrapper for the MailerLite API";
	this.version      = "0.1.0";
	this.cfmapping    = "cfmailerlite";
	this.dependencies = [];

	function configure(){
		settings = {
			baseURL : "https://api.mailerlite.com/api/v2/",
			apiKey  : "REQUIRED"
		};
	}
}